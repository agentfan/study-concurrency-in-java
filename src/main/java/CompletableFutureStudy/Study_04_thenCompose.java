package CompletableFutureStudy;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Study_04_thenCompose {

    private static CompletableFuture<Integer> getSum(int first, int second) {
        return CompletableFuture.supplyAsync(() -> first + second);
    }

    private static CompletableFuture<Integer> getMul(int first, int second) {
        return CompletableFuture.supplyAsync(() -> first * second);
    }

    private static CompletableFuture<CompletableFuture<Integer>> getSumThenMul(int first, int second, int third) {
        return getSum(first, second).thenApply(sum -> getMul(sum, third));
    }

    private static CompletableFuture<Integer> getComposeSumThenMul(int first, int second, int third) {
        return getSum(first, second).thenCompose(sum -> getMul(sum, third));
    }

    public static void main(String... args) throws ExecutionException, InterruptedException {
        System.out.println("Start computations with thenApply()...");
        CompletableFuture<CompletableFuture<Integer>> comlexResult = getSumThenMul(1,2,3);
        CompletableFuture<Integer> result = comlexResult.join();
        System.out.printf("Finished: (1 + 2) * 3 = %d%n", result.join());

        System.out.println("Start computations with thenCompose()...");
        result = getComposeSumThenMul(1, 2, 3);
        System.out.printf("Finished: (1 + 2) * 3 = %d%n", result.join());
    }
}
