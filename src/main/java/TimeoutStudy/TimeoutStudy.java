package TimeoutStudy;

import java.util.concurrent.*;

public class TimeoutStudy {

    private static int GET_TIMEOUT_SECONDS = 3;
    private static int TIMER_TIMEOUT_SECONDS = 5;

    public static void main(String... args) throws InterruptedException, ExecutionException, TimeoutException {
        System.out.println("Start app");
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(2);
        try {
            CompletableFuture<Integer> future = CompletableFuture.supplyAsync(TimeoutStudy::longProcessWithPause, executor);
            executor.schedule(() -> timeoutFuture(future), TIMER_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            int counter = future.get(GET_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            System.out.printf(String.format("Counter = %d%n", counter));
        } catch(TimeoutException e){
            System.out.println("Get() got timeout!!!");
        }
        executor.shutdown();
        executor.awaitTermination(30, TimeUnit.SECONDS);
    }

    private static int longProcessWithPause() {
        int counter = 0;
        try {
            System.out.println("Start counting...");
            while (!Thread.currentThread().isInterrupted() && counter < 20) {
                Thread.sleep(500);
                System.out.println(String.format("Count %d", ++counter));
            }
            if (Thread.currentThread().isInterrupted()) {
                System.out.println("Got interrupred flag!!!");
            }
        } catch (InterruptedException e) {
            System.out.println("I'm interrupted!!!");
            Thread.currentThread().interrupt();
        }
        return counter;
    }

    private static void timeoutFuture(CompletableFuture<Integer> future) {
        synchronized (future) {
            if (!future.isDone()) {
                System.out.println("Timeout!!!");
                future.complete(-1);
            }
        }
    }
}
