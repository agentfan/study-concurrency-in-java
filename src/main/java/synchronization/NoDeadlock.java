package synchronization;

class CleverWorker extends Thread {

    private final String name;
    private final CleverResource from;
    private final CleverResource to;
    private final int power;

    CleverWorker(String name, int power, CleverResource from, CleverResource to) {
        this.name = name;
        this.power = power;
        this.from = from;
        this.to = to;
    }

    @Override
    public void run() {
        try {
            System.out.printf("%s starts working!!!%n", name);
            int value;
            while ((value = from.get()) != 0) {
                System.out.printf("%s get %d%n", name, value);
                sleep(power);
                value = to.put();
                System.out.printf("%s put %d%n", name, value);
            }
            System.out.printf("%s ends working!!!%n", name);
        } catch (InterruptedException e) {
        }
    }
}

class CleverResource {
    private int value;

    CleverResource(int value) {
        this.value = value;
    }

    public synchronized int get() {
        return value > 0 ? value-- : 0;
    }

    public synchronized int put() {
        return ++value;
    }
}

public class NoDeadlock {

    public static void main(String... args) {
        System.out.println("Study deadlock solution");
        CleverResource resource1 = new CleverResource(10);
        CleverResource resource2 = new CleverResource(10);
        CleverWorker worker1 = new CleverWorker("Jhon", 200, resource1, resource2);
        CleverWorker worker2 = new CleverWorker("Sam", 100, resource2, resource1);
        worker1.start();
        worker2.start();
    }
}